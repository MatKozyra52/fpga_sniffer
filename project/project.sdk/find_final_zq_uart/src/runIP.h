/*
 * runIP.h
 *
 *  Created on: 15.06.2022
 *      Author: Acer
 */

#ifndef SRC_RUNIP_H_
#define SRC_RUNIP_H_

#define FIND_MODE_ALL		3
#define FIND_MODE_LETTER	2
#define FIND_MODE_NUMBERS	1

#define CONTROL_REG_START(settings)						((u32)settings | (u32)(0x00000001))
#define CONTROL_REG_STOP(settings)						((u32)settings & (u32)(0xFFFFFFFE))
#define CONTROL_REG_TEST_MODE(settings, mode)			(((u32)mode & (u32)(0x00000003)) << 2) | (u32)settings
#define CONTROL_REG_SEQ_LEN(settings, len)				(((u32)len & (u32)(0x000000ff)) << 8) | (u32)settings
#define CONTROL_REG_STRICT_LEN(settings, strict)		(((u32)strict & (u32)(0x00000001)) << 4) | (u32)settings

#define SETTINGS_GET_STRICT_LEN(settings)				(((u32)settings & (u32)(0x00000010)) >> 4)

#define INPUT_REG(pattern_char, char_in) 				((((u32)pattern_char & (u32)(0x000000FF)) << 16) | ((u32)char_in & (u32)(0x000000FF)))
#define STATUS_REG_READY(data) 							((u32)data & (u32)(0x00000001))
#define STATUS_REG_FOUND(data) 							(((u32)data & (u32)(0x00000002)) >> 1)
#define STATUS_REG_MATCH_PATTERN(data) 					(((u32)data & (u32)(0x00000004)) >> 2)
#define RESULT_REG_FIRST(param) 						((u32)param & (u32)(0x000000FF))
#define RESULT_REG_LAST(param) 							(((u32)param & (u32)(0x00FF0000)) >> 16 )

void find_final_char(u32 *setting, char char_in, char pattern_char, u32 *result, u32 *status);

u32 find_final_settings(u32 testmode, u32 len, u32 strict);

#endif /* SRC_RUNIP_H_ */
