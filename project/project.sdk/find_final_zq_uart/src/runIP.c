#include "xparameters.h"
#include "xil_io.h"
#include "find_final_ip.h"
#include "string.h"
#include <stdio.h>
#include "runIP.h"


/**************************** user definitions ********************************/
#define CHANNEL 1
//Processor base addres redefinition
#define IP_BASE_ADDR XPAR_FIND_FINAL_IP_0_S00_AXI_BASEADDR
//Processor registers' offset redefinition
#define CONTROL_REG_OFFSET 		FIND_FINAL_IP_S00_AXI_SLV_REG0_OFFSET
#define INPUT_REG_OFFSET 		FIND_FINAL_IP_S00_AXI_SLV_REG1_OFFSET
#define STATUS_REG_OFFSET 		FIND_FINAL_IP_S00_AXI_SLV_REG2_OFFSET
#define RESULT_REG_OFFSET 		FIND_FINAL_IP_S00_AXI_SLV_REG3_OFFSET


void find_final_char(u32 *settings, char char_in, char pattern_char, u32 *result, u32 *status){

	//Send data to data register of  processor.
	u32 data = INPUT_REG(pattern_char, char_in);
	FIND_FINAL_IP_mWriteReg(IP_BASE_ADDR, INPUT_REG_OFFSET, data);

	//Start  processor - pulse start bit in control register
	data = CONTROL_REG_START(*settings);
	FIND_FINAL_IP_mWriteReg(IP_BASE_ADDR, CONTROL_REG_OFFSET, data);

	//Wait for ready bit in status register
	while( STATUS_REG_READY(FIND_FINAL_IP_mReadReg(IP_BASE_ADDR, STATUS_REG_OFFSET)) == 0);

	//Get and chack results
	*result = FIND_FINAL_IP_mReadReg(IP_BASE_ADDR, RESULT_REG_OFFSET);

	*status = FIND_FINAL_IP_mReadReg(IP_BASE_ADDR, STATUS_REG_OFFSET);

	//start to 0
	data = CONTROL_REG_STOP(*settings);
	FIND_FINAL_IP_mWriteReg(IP_BASE_ADDR, CONTROL_REG_OFFSET, data);


}

u32 find_final_settings(u32 testmode, u32 len, u32 strict){
	u32 settings = 0;
	settings = CONTROL_REG_TEST_MODE(settings, testmode);
	settings = CONTROL_REG_SEQ_LEN(settings, len);
	settings = CONTROL_REG_STRICT_LEN(settings, strict);
	return settings;
}
