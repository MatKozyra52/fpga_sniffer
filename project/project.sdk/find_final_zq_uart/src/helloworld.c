
/***************************** Include Files *********************************/

#include "xparameters.h"
#include "xuartps.h"
#include <stdio.h>
#include "xil_printf.h"
#include "find_final_ip.h"
#include "string.h"
#include "runIP.h"
#include "xgpiops.h"
#include "xstatus.h"
#include "sleep.h"

/************************** Constant Definitions *****************************/

#define UART_DEVICE_ID0                 XPAR_XUARTPS_0_DEVICE_ID
#define UART_DEVICE_ID1                 XPAR_XUARTPS_1_DEVICE_ID
#define BUFF_SIZE						128


/************************** Function Prototypes ******************************/

int UartPsConfig(XUartPs *InstancePtr, u16 DeviceId, u32 BaudRate);
u32 API_config(char *pattern);

/************************** Variable Definitions *****************************/

XGpioPs Gpio;	/* The driver instance for GPIO Device. */
XGpioPs_Config *ConfigPtr;

/*****************************************************************************/
/**
*
* Main function to call the Hello World example.
*
* @param	None
*
* @return
*		- XST_FAILURE if the Test Failed .
*		- A non-negative number indicating the number of characters
*		  sent.
*
* @note		None
*
******************************************************************************/


int main(void)
{
	XUartPs Uart_PS_0;
	XUartPs Uart_PS_1;

	u8 char_in;
	char pattern[32];
	u8 first, last;

	/* Initialize the GPIO driver. */
	ConfigPtr = XGpioPs_LookupConfig(XPAR_XGPIOPS_0_DEVICE_ID);
	int Status = XGpioPs_CfgInitialize(&Gpio, ConfigPtr,
					ConfigPtr->BaseAddr);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	//GPIO_PS set direction
	XGpioPs_SetDirectionPin(&Gpio, 50, 0);
	XGpioPs_SetOutputEnablePin(&Gpio, 50, 0);
	XGpioPs_SetDirectionPin(&Gpio, 7, 1);
	XGpioPs_SetOutputEnablePin(&Gpio, 7, 1);

	/* UART INIT */
	Status = UartPsConfig(&Uart_PS_0, UART_DEVICE_ID0, 9600);
	if (Status == XST_FAILURE) {
		xil_printf("Uart_PS_0 Failed\r\n");
		return XST_FAILURE;
	}

	Status = UartPsConfig(&Uart_PS_1, UART_DEVICE_ID1, 9600);
	if (Status == XST_FAILURE) {
			xil_printf("Uart_PS_1 Failed\r\n");
			return XST_FAILURE;
	}


	print("Hello Friend\n\r");
	u32 settings = API_config(pattern);


	char output[BUFF_SIZE] = "";
	u32 pattern_pos = 0;
	u32 status;
	u32 result;

	while(1){
		while(0 == (XUartPs_Recv(&Uart_PS_0, &char_in, 1))){
			if(XGpioPs_ReadPin(&Gpio, 50)) settings = API_config(pattern);
		}

		find_final_char(&settings, char_in, pattern[pattern_pos], &result, &status);


		if(STATUS_REG_MATCH_PATTERN(status)){
			if(pattern_pos < strlen(pattern)) pattern_pos++;
			strncat(output, &char_in, 1);
		}else{
			pattern_pos = 0;
			memset(output, 0, BUFF_SIZE);
		}

		if(STATUS_REG_FOUND(status)){
			first = RESULT_REG_FIRST(result);
			last = RESULT_REG_LAST(result);
			u8 diff = last - first + 1;
			//printf("len = %d, l= %d f= %d diff %d\n",strlen(output), last, first, (strlen(output) - diff));
			char *seq_pointer = output + (strlen(output) - diff);
			if(pattern_pos == 0 || ((diff + strlen(pattern)) >= strlen(output))) printf("%s\n", seq_pointer);
		}


	}



	return Status;
}

/* LOCAL FUNCTIONS */

int UartPsConfig(XUartPs *InstancePtr, u16 DeviceId, u32 BaudRate)
{
	int Status;
	XUartPs_Config *Config;

	Config = XUartPs_LookupConfig(DeviceId);
	if (NULL == Config) {
		return XST_FAILURE;
	}

	Status = XUartPs_CfgInitialize(InstancePtr, Config, Config->BaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	XUartPs_SetBaudRate(InstancePtr, BaudRate);

	return XST_SUCCESS;
}

u32 API_config(char *pattern){

	XGpioPs_WritePin(&Gpio, 7, 1);
	u32 mode = 0;
	print("Configuration\n\r>> 1 - select pattern \n\r>> 2 - without pattern\r\n");
	scanf("%d", &mode);
	memset(pattern, 0, 10);
	if(2 != mode){
		print("Pattern:\n\r");
		scanf("%s", pattern);
	}

	print("Data type\n\r>> 1 - ASCII NUMBERS \n\r>> 2 - ASCI LETTERS\r\n>> 3 - ALL ASCI CHARS\r\n");
	scanf("%d", &mode);

	print("*Optional - minimal seq len\n\r");
	u32 len;
	scanf("%d", &len);

	u32 strict = 0;
	if(len > 0){
		print("*Last question - strict to this len?\n\r0-NO\n\r1-YES\n\r");
		scanf("%d", &strict);
	}



	u32 settings = find_final_settings(mode, len, strict);
	printf("Happy sniffing %d %d %d %s\n\r", mode, len, strict, pattern);

	XGpioPs_WritePin(&Gpio, 7, 0);

	return settings;
}
