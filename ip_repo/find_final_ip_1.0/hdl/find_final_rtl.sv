module find_final_rtl( clock, reset, start, test_char_mode, char_in, pattern_char, seq_len, strict_len, ready, found, match_pattern, first, last);
//PARAMS
parameter integer ADDRESS_W = 8;

localparam MODE_TEST_NUMBER = 2'b01;
localparam MODE_TEST_LETTER = 2'b10;
localparam MODE_TEST_ALL = 2'b11;

//CONST
localparam FIRST_ASCI = 8'h21;   //first all ascii sign      
localparam LAST_ASCI = 8'h7E;   //last all ascii sign 
localparam FIRST_NUMBER = 8'h30;   //first number ascii sign      
localparam LAST_NUMBER = 8'h39;   //last number ascii sign 
localparam FIRST_L_LETTER = 8'h61;   //first lowercase letter ascii sign      
localparam LAST_L_LETTER = 8'h7A;   //last lowercase letter ascii sign 
localparam FIRST_U_LETTER = 8'h41;   //first uppercase ascii sign      
localparam LAST_U_LETTER = 8'h5A;   //last uppercase ascii sign 

//IN/OUT
input clock, reset;
input start; //start processing
input [1:0] test_char_mode;
input [7:0] char_in;
input [7:0] pattern_char;
input [7:0] seq_len;
input strict_len;
output reg ready; //machine is ready
output reg found; //result is ready
output reg match_pattern; //get next pattern_char
output reg [ADDRESS_W - 1:0] first, last;

//STATES
localparam S0 = 4'h00, S1 = 4'h01, S2 = 4'h02, S3 = 4'h03, S4 = 4'h04, S5 = 4'h05,
 S6 = 4'h06, S7 = 4'h07, S8 = 4'h08, S9 = 4'h09, S10 = 4'h0a,
 S11 = 4'h0b, S12 = 4'h0c, S13 = 4'h0d, S14 = 4'h0e, S15 = 4'h0f;
reg [3:0] state;

//Algorithm Variables
reg signed [ADDRESS_W-1:0] pos, len;
reg signed [7:0] single_char;

//Iterators
reg [3:0] i, d;
always @ (posedge clock)
begin
    if(reset==1'b1)
    begin
        match_pattern <= 1'b0;
        found <= 1'b0;
        ready <= 1'b0;
        state <= S0;
        len <=0;
        pos <=0;
        first <= 0;
        last <= 0;
    end
    else
    begin
        case(state)
        S0: begin
            found = 0;
            if(start == 1'b1) state <= S1; else state <= S0;
            end
        S1: begin
            ready <= 1'b0;
            single_char <= char_in;
            state <= S2;
            end
        S2: begin
            if (pattern_char == single_char) 
                state <= S10;
            else
                if (pattern_char != 0)
                    state <= S11;
                else
                    if (
                    ((MODE_TEST_ALL == test_char_mode) && single_char >= FIRST_ASCI && single_char <= LAST_ASCI) || 
                    ((MODE_TEST_LETTER == test_char_mode) && ((single_char >= FIRST_L_LETTER && single_char <= LAST_L_LETTER) || (single_char >= FIRST_U_LETTER && single_char <= LAST_U_LETTER))) ||
                    ((MODE_TEST_NUMBER == test_char_mode) && single_char >= FIRST_NUMBER && single_char <= LAST_NUMBER)
                    )
                        state <= S3;
                    else 
                        state <= S4;
            end
        S3: begin
            len++;
            match_pattern = 1;
            state <= S5;
            end
        S4: begin
            len <= 0;
            match_pattern <= 0;
            state <= S5;
            end
        S5: begin
            if(len > seq_len)
                if(1 == strict_len)
                    state <= S6;
                else
                    state <= S7;
            else
                if(len == 1)
                    state <= S8;
                else
                    state <= S9;
            end
        S6: begin
            first++;
            state <= S7;
            end
        S7: begin
            last++;
            state <= S13;
            end
        S8: begin
            first <= pos;
            state <= S9;
            end
        S9: begin
            last <= pos;
            if(len == seq_len) state <= S13; else state <= S12;
            end
        S10: begin
            match_pattern <= 1;
            state <= S12;
            end
        S11: begin
            match_pattern <= 0;
            state <= S12;
            end
        S12: begin
            found = 0;
            state <= S14;
            end
        S13: begin
            found = 1;
            state <= S14;
            end
        S14: begin
            pos++;
            state <= S15;
            end
        S15: begin
            ready <= 1'b1;
            if(start == 1'b0) state <= S0; else state <= S15;
            end
        endcase
    end
end
endmodule
