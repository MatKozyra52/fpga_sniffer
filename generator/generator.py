import serial
import random
import keywords as kw
dev = serial.Serial('COM5', 9600)

file = open("rand_data_1K.raw", "rb")
raw_data = file.read()
file.close()

num_of_elements = 0
for i in range(len(kw.KEYWORDS)):
    num_of_elements += len(kw.VALUES[kw.KEYWORDS[i]])

places = []
for i in range(num_of_elements):
    places.append(random.randint(0,len(raw_data)))

places.sort()
print(places)

ready_file = b''
n = 0
m = 0
k = 0
for i,b in zip(range(len(raw_data)), raw_data):
    if i == places[k]:
        ready_file += kw.KEYWORDS[n]
        ready_file += kw.VALUES[kw.KEYWORDS[n]][m]
        m += 1
        if m == len(kw.VALUES[kw.KEYWORDS[n]]):
            n += 1
            m = 0
        if k < len(places) - 1:
            k += 1
        print(n, m, k)
        
    ready_file += raw_data[i].to_bytes(1,'little')

print(ready_file)
dev.write(ready_file)
dev.close()